package institute;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author rushil
 */
public class test {
    public static void main(String[] args) {
        Student s1 = new Student("John", 1, "CSE");
        Student s2 = new Student("Nick", 3, "CSE");
        Student s3 = new Student("Mia", 3, "EE");
        Student s4 = new Student("Jonathan", 4, "EE");
        
        List <Student> cseStudents = new ArrayList<Student>();
        cseStudents.add(s1);
        cseStudents.add(s2);
        
        List <Student> eeStudents = new ArrayList<Student>();
        eeStudents.add(s3);
        eeStudents.add(s4);
        
        Department CSE = new Department("CSE", cseStudents);
        Department EE = new Department("EE", eeStudents);
        
        List <Department> departments = new ArrayList<Department>();
        departments.add(CSE);
        departments.add(EE);
        
        Institute institute = new Institute("Sheridan", departments);
        System.out.print("Total number of students in Sheridan : ");
        System.out.print(institute.getTotalStudentsInInstitute());
    }
}
