package institute;

/**
 *
 * @author rushil
 */
public class Student {
    String name;
    int id;
    String dept;
    
    Student(String name, int id, String dept) {
        this.name = name;
        this.id = id;
        this.dept = dept;
    }
    
    public String getName() {
        return this.name;
    }
    
    public int getId() {
        return this.id;
    }
    
    public String getDept() {
        return this.dept;
    }
}
